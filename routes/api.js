var express = require('express');
var router = express.Router();
var dal = require('../dataAccessLayer');

// a data api
router.get('/example', function(req, res) {
    dal.getExampleItemData()
    .then(function(result) {
        res.json(result);
    })
    .fail(function(err) {
        res.status(500).send(result);
    });

});

// router.get('/daily-sold', function(req, res) {
//     dal.getDailySold()
//     .then(function(result) {
//         res.json(result);
//     })
//     .fail(function(err) {
//         res.status(500).send(result);
//     })
// })

// router.get('/average-daily-per-customer', function(req, res){
//     dal.getAverageDailyPerCustomer()
//     .then(function(result) {
//         res.json(result);
//     })
//     .fail(function(err) {
//         res.status(500).send(result);
//     })
// })

// router.get('/total-daily-items-sales', function(req, res) {
//     dal.getTotalDailyItemsSales()
//     .then(function(result) {
//         res.json(result);
//     })
//     .fail(function(err) {
//         res.status(500).send(result);
//     })
// })

module.exports = router;