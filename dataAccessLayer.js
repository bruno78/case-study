var Q = require('q');
var fs = require("fs");

// Parsing data from CSV to array of objects
var dataParser = function() {
    var deferred = Q.defer();
    
    d3.csv('data/data.csv', function(error, data) {
        if(error) {
            console.log(error);
        }

        // it will iterate over count parsing into num value
        data.forEach(function(d) {
            d.count = +d.count;
        })
        deferred.resolve(data);
    });
    return deferred.promise;
};

// get total of pudding items sold daily 
// var getDailySold = function() {
//     var deferred = Q.defer();
//     var data = dataParser()
//                 .then(function(result) {
//                     d3.nest() 
//                     .key(function(d) { return d.date; })
//                     .rollup(function(v) { return d3.sum(v, function(d) {return d.count})})
//                     .entries(data);
//                 });

//     deferred.resolve(data);

//     return deferred.promise;
// }

// // Get Average daily pudding items sold per customer
// var getAverageDailyPerCustomer = function() {
//     var deferred = Q.defer();
//     var data = dataParser();
//     var result = d3.nest()
//             .key(function(d){ return d.date; })
//             .key(function(d){ return d.customerId;})
//             .rollup(function(v){return d3.mean(v, function(d) {return d.count;})})
//             .entries(data);

//     deferred.resolve(result);
    
//     return deferred.promise;
// }

// // Get Total daily item sales
// var getTotalDailyItemsSales = function() {
//     var deferred = Q.defer();
//     var data = dataParser();
//     var result = d3.nest()
//         .key(function(d) { return d.date; })
//         .key(function(d) { return d.item; })
//         .rollup(function(v) { return d3.sum(v, function(d) { return d.count; })})
//         .entries(data);
    
//     deferred.resolve(result);

//     return deferred.promise;
// }

// Original working method:
// var getExampleItemData = function() {
//     var deferred = Q.defer();
    
//     var result = [
//         {key: "2016-06-02", values: 10},
//         {key: "2016-06-03", values: 15},
//         {key: "2016-06-04", values: 16},
//         {key: "2016-06-05", values: 17},
//         {key: "2016-06-06", values: 25},
//         {key: "2016-06-07", values: 5},

//     ];

//     deferred.resolve(result);

//     return deferred.promise;
// }

// This is for test
var getExampleItemData = function() {
    var deferred = Q.defer();
    
    var result = dataParser()

    deferred.resolve(result);

    return deferred.promise;
}

module.exports = {
    // export methods here
    getExampleItemData: getExampleItemData,

    // getDailySold: getDailySold,
    // getAverageDailyPerCustomer: getAverageDailyPerCustomer,
    // getTotalDailyItemsSales: getTotalDailyItemsSales
};
