myApp.factory('DataService', ['$http', function($http) {
    
    // App configurations
    var getExampleData = function() {
        return $http.get("/api/example", null);
    };

    // var getDailySoldData = function() {
    //     return $http.get("/api/daily-sold", null);
    // }

    // var getAverageDailyPerCustomerData = function() {
    //     return $http.get("/api/average-daily-per-customer", null);
    // }

    // var getTotalDailyItemsSalesData = function() {
    //     return $http.get("/total-daily-items-sales");
    // }
    return {
        // app and them configurations
        getExampleData: getExampleData,
        // getDailySoldData: getDailySoldData,
        // getAverageDailyPerCustomerData: getAverageDailyPerCustomerData,
        // getTotalDailyItemsSalesData: getTotalDailyItemsSalesData,
    };
}]);