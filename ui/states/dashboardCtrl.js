angular.module('myApp')
.controller('dashboardCtrl', ['$scope', 'DataService', function($scope, DataService) {

    $scope.lineChart = {};
    $scope.lineChart.labelX = "Date";
    $scope.lineChart.labelY = "# Items Sold";
    $scope.lineChart.guidelinesX = false;
    $scope.lineChart.guidelinesY = true;
    $scope.lineChart.formatY = "f";
    $scope.lineChart.legend = "bottom";

    DataService.getExampleData().then(function(data) {
        console.log("Data", data);
        $scope.lineChart.data = [{key: "Sales", values: data.data}];
    }).catch(function(err) {
        console.error("There was an error");
    })
    
    // total pudding sold daily
    // $scope.totalPudding = {};
    // $scope.totalPudding.labelX = "Date";
    // $scope.totalPudding.labelY = "# Puddings Sold";
    // $scope.totalPudding.guidelinesX = false;
    // $scope.totalPudding.guidelinesY = true;
    // $scope.totalPudding.formatY = "f";
    // $scope.totalPudding.legend = "bottom";

    // DataService.getDailySoldData().then(function(data) {
    //     console.log("Data", data);
    //     $scope.totalPudding.data = [{key: "Sales", values: data.data}];
    // }).catch(function(err) {
    //     console.error("There was an error");
    // })

    // average puddding sold per customer
    // $scope.averagePerCustomer = {};
    // $scope.averagePerCustomer.labelX = "Date";
    // $scope.averagePerCustomer.labelY = " Average";
    // $scope.averagePerCustomer.guidelinesX = false;
    // $scope.averagePerCustomer.guidelinesY = true;
    // $scope.averagePerCustomer.formatY = "f";
    // $scope.averagePerCustomer.legend = "bottom";

    // DataService.getAverageDailyPerCustomerData().then(function(data) {
    //     console.log("Data", data);
    //     $scope.averagePerCustomer.data = [{key: "Date", values: data.data}];
    // }).catch(function(err) {
    //     console.error("There was an error");
    // })

    // daily puddings per pudding name
    // $scope.dailyItems = {};
    // $scope.dailyItems.labelX = "Date";
    // $scope.dailyItems.labelY = "# Items Sold";
    // $scope.dailyItems.guidelinesX = false;
    // $scope.dailyItems.guidelinesY = true;
    // $scope.dailyItems.formatY = "f";
    // $scope.dailyItems.legend = "bottom";

    // DataService.getTotalDailyItemsSalesData().then(function(data) {
    //     console.log("Data", data);
    //     $scope.dailyItems.data = [{key: "Date", values: data.data}];
    // }).catch(function(err) {
    //     console.error("There was an error");
    // })



}])
