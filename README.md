## Synopsis

Brauss is a large dairy products company based in Berlin, Germany. Brauss has been paying close attention to a recently launched line of products: Chocolate puddings. As such, they have started building a web-based dashboard that will help them monitor the sales of chocolate pudding on a monthly basis.

There is an important executive board meeting in 3 hours, and the board would like to use the dashboard to understand chocolate pudding performance. Unfortunately, their full-stack developer has recently quit his job and left them with an undocumented code base. The CEO's assistant has called you to add some charts to the dashboard to give some insights on pudding sales in early June.

You have received source code and a data export from an external database (IT helped him with that) in a file called data/data.csv. The executives would like to see several simple line charts, as described in the details below.

## Installation

To install packages: npm install
To start the application: npm start

## Guidance

* Brauss have no time for database setup. So you will have to find a way to load in the data file. Feel free to change the file format, but the data column names and data values cannot be changed manually.
* All data transformations or aggregations should occur on the server side.
* The executives would like to see several line charts in the dashboard:
1) Total daily pudding items sold
2) Average daily pudding items sold per customer
3) Total daily item sales, split into a line for each pudding item (Dani, Milki etc)
* KEY FILES: dataAccessLayer.js, api.js, ui/states/dashboardCtrl.js, ui/states/dashboard.html
* Please send a zipped project folder back to the CEO's assistant. He is tech savy and will know how to run the dashboard on his computer.
* Provide some comments below about how this application should be better designed.

Provide comments below:
* Better naming for folders: services instead of data_manager, http_manager. common appears twice, change both of them to a better name. 
* A better code design would be "The Specific Style":
  * Where fonts, images, and stylesheets would be in the assets folder
  * A JS folder containing a separate folder for each part: controllers, directives, filters, routes, services.
    * Move HttpMiddleWareService.js and DataService.js to Services folder
    * Move aacoc_charts to Directives folder
    * Move abstractSTateCtr.js and dashboardCtrl.js to Contorllers folder 
    * Move main_page to JS folder
    * Move errorProvider.js and loggingHandler.js here (Maybe create a config folder and move them there? - I wasn't sure)
  * A lib folder containing external or 3rd party scripts
    * move Vendor folder here
  * Partials folder with all the html
    * Move dashboard.html to Partials 

- The entire structure was hard to for me to understand but here some of my solutions to handle the 3 parts of data coming from parsing the CSV data into JSON ``` var data = d3.csv(data) ```.

- Implementation for sum total daily pudding items sold:
```javascript

var dailySold = d3.nest()
  .key(function(d) { return d.date; })
  .rollup(function(v){return d3.sum(v, function(d) {return d.count})})
  .entries(data);

```

- Implementation of Average daily pudding items sold per customer:

```javascript

var averageDailyPerCustomer = d3.nest()
  .key(function(d){ return d.date; })
  .key(function(d){ return d.customerId;})
  .rollup(function(v){return d3.mean(v, function(d) {return d.count;})})
  .entries(data)
```

- Implementation of Total daily item sales, split into a line for each pudding item (Dani, Milki etc)

```javascript

var totalDailyItemsSales = d3.nest()
  .key(function(d) { return d.item; })
  .key(function(d) { return d.date; })
  .rollup(function(v){ return d3.sum(v, function(d) {return d.count;})
  .entries(data)
  })
```